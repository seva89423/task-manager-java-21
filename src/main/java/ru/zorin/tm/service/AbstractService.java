package ru.zorin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.repository.IRepository;
import ru.zorin.tm.api.service.IService;
import ru.zorin.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> iRepository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.iRepository = repository;
    }

    @Nullable
    @Override
    public void load(@Nullable List<E> list) {
        if (list == null) return;
        iRepository.load(list);
    }

    public List<E> getList() {
        return iRepository.findAll();
    }
}
