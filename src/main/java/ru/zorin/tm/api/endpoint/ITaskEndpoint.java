package ru.zorin.tm.api.endpoint;

import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception;

    @WebMethod
    void addTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "task", partName = "task") Task task) throws Exception;

    @WebMethod
    void removeTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "task", partName = "task") Task task) throws Exception;

    @WebMethod
    void loadTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "taskList", partName = "taskList") List<Task> tasks) throws Exception;

    @WebMethod
    List<Task> getTaskList();

    @WebMethod
    List<Task> findAllTask(@WebParam (name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Task findOneById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception;

    @WebMethod
    Task findOneByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception;

    @WebMethod
    Task findOneByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception;

    @WebMethod
    Task updateTaskById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id, @WebParam(name = "name", partName = "name") String name, @WebParam(name = "description", partName = "description") String description) throws Exception;

    @WebMethod
    Task removeOneByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception;

    @WebMethod
    Task removeOneByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception;

    @WebMethod
    Task removeOneById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception;

    @WebMethod
    Task updateTaskByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index, @WebParam(name = "name", partName = "name") String name, @WebParam(name = "description", partName = "description") String description) throws Exception;
}