package ru.zorin.tm.api.endpoint;

import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @WebMethod
    List<User> findAll(@WebParam (name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    User createUser(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password) throws Exception;

    @WebMethod
    User createUserEmail(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password,@WebParam(name = "email", partName = "email") String email) throws Exception ;

    @WebMethod
    User createUserRole(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password,@WebParam(name = "role", partName = "role") Role role) throws Exception ;

    @WebMethod
    User findUserById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception ;

    @WebMethod
    User findUserByLogin(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception ;

    @WebMethod
    User updateUserById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password) throws Exception ;

    @WebMethod
    User updateUserByLogin(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password) throws Exception ;

    @WebMethod
    User removeUserOne(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "user", partName = "user") User user) throws Exception;

    @WebMethod
    User removeUserOneById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception;

    @WebMethod
    User removeUserByLogin(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception;
}