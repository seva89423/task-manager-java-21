package ru.zorin.tm.api.endpoint;

import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminUserEndpoint {

    @WebMethod
    User createUserWithEmail(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, @WebParam(name = "email", partName = "email") String email) throws Exception;

    @WebMethod
    User createUserWithRole(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, @WebParam(name = "role", partName = "role") Role role) throws Exception;

    @WebMethod
    User lockUser(@WebParam(name = "session", partName = "session") Session session,  @WebParam(name = "login", partName = "login") String login) throws Exception;

    @WebMethod
    User unlockUser(@WebParam(name = "session", partName = "session") Session session,  @WebParam(name = "login", partName = "login") String login) throws Exception;

    @WebMethod
    User removeUserByLog(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception;

}