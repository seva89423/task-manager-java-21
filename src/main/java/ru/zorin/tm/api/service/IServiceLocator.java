package ru.zorin.tm.api.service;

public interface IServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IDomainService getDomainService();

    IProjectService getProjectService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

    IAdminService getAdminService();

}