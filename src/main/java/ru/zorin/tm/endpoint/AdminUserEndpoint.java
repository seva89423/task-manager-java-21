package ru.zorin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.endpoint.IAdminUserEndpoint;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    @Override
    public User createUserWithEmail(@WebParam(name = "session", partName = "session") @Nullable Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, @WebParam(name = "email", partName = "email") String email) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @WebMethod
    @Nullable
    @Override
    public User createUserWithRole(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, @WebParam(name = "role", partName = "role") Role role) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @WebMethod
    @Nullable
    @Override
    public User lockUser(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockServiceByLogin(login);
    }

    @WebMethod
    @Nullable
    @Override
    public User unlockUser(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unlockServiceByLogin(login);
    }

    @WebMethod
    @Override
    public User removeUserByLog(@WebParam(name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeByLogin(login);
    }
}