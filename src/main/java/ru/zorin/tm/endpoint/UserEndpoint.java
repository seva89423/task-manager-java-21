package ru.zorin.tm.endpoint;

import ru.zorin.tm.api.endpoint.IUserEndpoint;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Override
    public List<User> findAll(@WebParam (name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    public User createUser(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password) throws Exception {
        return serviceLocator.getUserService().create(login, password);
    }

    @WebMethod
    @Override
    public User createUserEmail(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, String email) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @WebMethod
    @Override
    public User createUserRole(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, Role role) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @WebMethod
    @Override
    public User findUserById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @WebMethod
    @Override
    public User findUserByLogin(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @WebMethod
    @Override
    public User updateUserById(@WebParam (name = "session", partName = "session") Session session, String id, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().updateById(id, login, password);
    }

    @WebMethod
    @Override
    public User updateUserByLogin(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().updateByLogin(login, login, password);
    }

    @WebMethod
    @Override
    public User removeUserOne(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "user", partName = "user") User user) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeUser(user);
    }

    @WebMethod
    @Override
    public User removeUserOneById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeById(id);
    }

    @WebMethod
    @Override
    public User removeUserByLogin(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "login", partName = "login") String login) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeByLogin(login);
    }
}