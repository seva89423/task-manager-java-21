package ru.zorin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.endpoint.ITaskEndpoint;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint implements ITaskEndpoint {

    private IServiceLocator serviceLocator;

    public TaskEndpoint() {

    }
    public TaskEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void createTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") @Nullable String name) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @WebMethod
    public void addTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "task", partName = "task") @Nullable Task task) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(session.getUserId(), task);
    }

    @WebMethod
    public void removeTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "task", partName = "task") @Nullable Task task) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().remove(session.getUserId(), task);
    }

    @WebMethod
    public void loadTask(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "taskList", partName = "taskList") List<Task> tasks) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().load(tasks);
    }

    @WebMethod
    public List<Task> getTaskList() {
        return serviceLocator.getTaskService().getList();
    }

    @WebMethod
    public List<Task> findAllTask(@WebParam (name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @WebMethod
    public Task findOneById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") @Nullable String id) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneById(session.getUserId(), id);
    }

    @WebMethod
    public Task findOneByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") @Nullable Integer index) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task findOneByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") @Nullable String name) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOneByName(session.getUserId(), name);
    }

    @WebMethod
    public Task updateTaskById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id, @WebParam(name = "name", partName = "name") String name, @WebParam(name = "description", partName = "description") String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @WebMethod
    public Task removeOneByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    public Task removeOneByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    public Task removeOneById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") @Nullable String id) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @WebMethod
    public Task updateTaskByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") @Nullable Integer index, @WebParam(name = "name", partName = "name") @Nullable String name, @WebParam(name = "description", partName = "description") @Nullable String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
    }
}