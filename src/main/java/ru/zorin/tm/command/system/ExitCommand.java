package ru.zorin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Exit application";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
      System.exit(0);
    }
}