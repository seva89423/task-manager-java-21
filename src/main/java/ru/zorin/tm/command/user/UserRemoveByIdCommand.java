package ru.zorin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete user using login";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = IServiceLocator.getUserService().removeByLogin(login);
        if (user == null) throw new InvalidLoginException();
        System.out.println("[REMOVED]");
        IServiceLocator.getAuthService().logout();
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}