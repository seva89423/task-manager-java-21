package ru.zorin.tm.command.data.fastxml.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.role.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataXmlFastClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-fast-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from xml (FasterXML) file";
    }

    @Nullable
    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML (FAST) DELETE]");
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}